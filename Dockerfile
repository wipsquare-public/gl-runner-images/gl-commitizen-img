FROM python:alpine
RUN apk add \
        openssh-client \
        git \
        yq \
        curl
RUN pip install --no-cache-dir --no-compile Commitizen
CMD [ "cz", "version" ]